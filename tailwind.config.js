module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#E13E49',
          darken: '#95171f',
        },
        secondary: {
          DEFAULT: '#2468F6',
          darken: '#0639a2',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
